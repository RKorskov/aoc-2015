// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-18 07:40:56 roukoru>

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

class SEmulator802 implements SEmulator {
    /**
     * A gate provides no signal until all of its inputs have a signal.
     */
    // Map <String,Integer> wires; // a..z, aa..zz, ...
    Set <SEGate> gates;
    Map <String,SEGateSwitch> wires;
    Set <String> ops;
    SEGate root; // wire 'a'
    Map <String, SEWire> sewire;
    Logger log;

    SEmulator802() {
        wires = new TreeMap <String,SEGateSwitch> ();
        root = null;
        gates = new HashSet <SEGate> ();
        ops = new HashSet <String> ();
        sewire = new TreeMap <String, SEWire> ();
        log = Logger.getGlobal();
    }

    public boolean emulate (final String[] ops) {
        construct(ops);
        return true;
    }

    public void print() {
        System.out.println ("Wire : Value");
        for (String k : wires.keySet())
            System.out.printf ("%s : %d\n", k, wires.get(k).get_output_value());
    }

    public int get_ip() {return -1;}

    public int get_wire (String widx) {
        if (wires.isEmpty() || !wires.containsKey (widx)) return -1;
        return wires.get(widx).get_output_value();
    }

    private void construct (final String[] ops) {
        /**
         * build circuit from operations list
         */
        List <String> lops = process_buffers (ops); // ops in List form
        for (String w : sewire.keySet())
            sewire.get(w).print();
        /*
        for (String op : ops)
            if (gate_and (op) ||
                gate_or (op)  ||
                gate_shl (op) ||
                gate_shr (op) ||
                gate_not (op) ||
                gate_buf (op) ||
                gate_switch (op)) continue;
            else return false;
        */
        System.out.println ("OPs:");
        for (String s : lops)
            System.out.println (s);
    }

    List <String> process_buffers (final String[] ops) {
        ArrayList <String> lops = new ArrayList <String> ();
        for (String op : ops) {
            if (gate_buf (op) || gate_switch (op))
                continue;
            else
                lops.add (op);
        }
        return lops;
    }

    private boolean gate_buf (final String op) {
        // 1 -> a
        /*
        SEGate 节门 = SEGateBuf.construct (op); // jie men
        if (节门 == null) return false;
        gates.add (节门);
        */
        Matcher m = SEmulator.pat_gate_buf.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("Buffer: " + op);
        String idx = m.group ("out");
        int val = Integer.parseInt (m.group ("value"));
        if (!sewire.isEmpty() && sewire.containsKey (idx))
            (sewire.get(idx)).set(val);
        else
            sewire.put (idx, new SEWire (idx, val));
        return true;
    }

    private boolean gate_switch (final String op) {
        /*
        SEGate 节门 = SEGateSwitch.construct (op);
        if (节门 == null) return false;
        gates.add (节门);
        */
        Matcher m = SEmulator.pat_gate_switch.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("Switch: " + op);
        String idx = m.group ("in");
        String out = m.group ("out");
        if (sewire.containsKey (idx))
            (sewire.get(idx)).add (out);
        else
            sewire.put (idx, new SEWire (idx, out));
        return true;
    }

    private boolean gate_not (final String op) {
        SEGate 节门 = SEGateNot.construct (op);
        if (节门 == null) return false;
        gates.add (节门);
        return true;
    }

    private boolean gate_shr (final String op) {
        SEGate 节门 = SEGateShr.construct (op);
        if (节门 == null) return false;
        gates.add (节门);
        return true;
    }

    private boolean gate_shl (final String op) {
        SEGate 节门 = SEGateShl.construct (op);
        if (节门 == null) return false;
        gates.add (节门);
        return true;
    }

    private boolean gate_and (final String op) {
        SEGate 节门 = SEGateAnd.construct (op);
        if (节门 == null) return false;
        gates.add (节门);
        return true;
    }

    private boolean gate_or (final String op) {
        SEGate 节门 = SEGateOr.construct (op);
        if (节门 == null) return false;
        gates.add (节门);
        return true;
    }

}
