// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-22 23:15:03 roukoru>

/*
  http://adventofcode.com/2015/day/9

Adventure of a サラリメン

--- Day 9: All in a Single Night ---

Every year, Santa manages to deliver all of his presents in a single night.

This year, however, he has some new locations to visit; his elves have provided him the distances between every pair of locations. He can start and end at any two (different) locations he wants, but he must visit each location exactly once. What is the shortest distance he can travel to achieve this?

For example, given the following distances:

London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141

The possible routes are therefore:

Dublin -> London -> Belfast = 982
London -> Dublin -> Belfast = 605
London -> Belfast -> Dublin = 659
Dublin -> Belfast -> London = 659
Belfast -> Dublin -> London = 605
Belfast -> London -> Dublin = 982

The shortest of these is London -> Dublin -> Belfast = 605, and so the answer is 605 in this example.

What is the distance of the shortest route?

--- Part Two ---

The next year, just to show off, Santa decides to take the route with the longest distance instead.

He can still start and end at any two (different) locations he wants, and he still must visit each location exactly once.

For example, given the distances above, the longest route would be 982 via (for example) Dublin -> London -> Belfast.

What is the distance of the longest route?
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
import java.util.Deque;
// import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
import java.util.Set;
// import java.util.TreeMap;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_09 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.SEVERE;
    final Level __LOG_LEVEL = Level.ALL; // wtf?!
    Logger log;

    final String[] Distances = {
        "Faerun to Norrath = 129",
        "Faerun to Tristram = 58",
        "Faerun to AlphaCentauri = 13",
        "Faerun to Arbre = 24",
        "Faerun to Snowdin = 60",
        "Faerun to Tambi = 71",
        "Faerun to Straylight = 67",
        "Norrath to Tristram = 142",
        "Norrath to AlphaCentauri = 15",
        "Norrath to Arbre = 135",
        "Norrath to Snowdin = 75",
        "Norrath to Tambi = 82",
        "Norrath to Straylight = 54",
        "Tristram to AlphaCentauri = 118",
        "Tristram to Arbre = 122",
        "Tristram to Snowdin = 103",
        "Tristram to Tambi = 49",
        "Tristram to Straylight = 97",
        "AlphaCentauri to Arbre = 116",
        "AlphaCentauri to Snowdin = 12",
        "AlphaCentauri to Tambi = 18",
        "AlphaCentauri to Straylight = 91",
        "Arbre to Snowdin = 129",
        "Arbre to Tambi = 53",
        "Arbre to Straylight = 40",
        "Snowdin to Tambi = 15",
        "Snowdin to Straylight = 99",
        "Tambi to Straylight = 70"
    };

    /*
    final String[] Distances = {
        "L to D = 464",
        "L to B = 518",
        "D to B = 141",
        "X to B = 5" // or 'X to L = m'
    };
    */

    public void help() {;}

    private String[] build_index (final String[] dists) {
        Set <String> dsts = new HashSet <String> ();
        for (String デ : dists) {
            Matcher マ = (サラリメン.pat).matcher (デ);
            if (マ == null || !マ.matches()) continue;
            dsts.add (マ.group(1));
            dsts.add (マ.group(2));
        }
        int 甲 = dsts.size();
        log.fine ("名字 " + 甲);
        return dsts.toArray (new String [甲]);
    }

    private int get_index (final String[] dists, final String dst) {
        for (int 号 = 0; 号 < dists.length; ++号)
            if (dists[号].compareTo (dst) == 0) return 号;
        return -1;
    }

    private サラリメン parse_input (final String[] dists) {
        String[] 名字 = build_index (dists);
        int[][] 长里 = new int [名字.length][];
        for (int 甲 = 0; 甲 < 名字.length; ++甲)
            长里[甲] = new int [名字.length];
        for (String 串 : dists) {
            Matcher マ = サラリメン.pat.matcher (串);
            if (マ == null || !マ.matches()) continue;
            String 原 = マ.group (1), // yuan
                目的 = マ.group (2); // mu de (di)
            int 长 = Integer.parseInt (マ.group (3)),
                位0 = get_index (名字, 原),
                位1 = get_index (名字, 目的);
            //log.warning ("原 " + 位0 + " 目的 " + 位1);
            长里[位0][位1] = 长;
        }
        return new サラリメン (名字, 长里);
    }

    private String[] read_lines (final String src) throws IOException {
        ArrayList <String> 矩阵 = new ArrayList <String> ();
        Scanner 输入流 = src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 7)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    public void eval (final String[] args) {
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        log.severe ("severe");
        log.warning ("warning");
        log.info ("info");
        log.config ("config");
        log.fine ("fine");
        log.finer ("finer");
        log.finest ("finest");
        try {
            サラリメン ンペ = parse_input (args.length <= 1 ? Distances : read_lines (args[1]));
            ンペ.print();
            System.out.println (process1 (ンペ));
            System.out.println (process2 (ンペ));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void print (final サラリメン dists) {
        int 甲 = 0;
        for (String 串 : dists.名字)
            System.out.println (++甲 + " : " + 串);
    }

    class WayPoint {
        final int 地南, 地东, 长里;
        WayPoint (int 南, int 东, int 里) {
            地南 = 南;
            地东 = 东;
            长里 = 里;
        }
    }

    private String deque2string (Deque <Integer> mway) {
        StringBuilder 串 = new StringBuilder();
        串.append ("大道");
        for (int 步 : mway)
            串.append (" " + 步);
        return 串.toString();
    }

    private int get_distance (final サラリメン dists, int 前步, int 步) {
        int p = dists.长里[前步][步],
            q = dists.长里[步][前步];
        if (p > 0) return p;
        if (q > 0) return q;
        return 0;
    }

    private int calculate_distance (final サラリメン dists, Deque <Integer> mway){
        int 道长 = -1, 前步 = 0;
        for (int 步 : mway) {
            //log.info ("跑步 " + dists.长里[前步][步] + " : " + dists.长里[步][前步]);
            if (道长 >= 0) {
                int 甲 = get_distance (dists, 前步, 步);
                if (甲 == 0) {
                    //log.info (String.format ("%d,%d = %d %b %s", 前步, 步, 甲, has_path_to (dists, 前步, 步), deque2string (mway)));
                    return -1;
                }
                道长 += 甲;
            }
            else
                道长 = 0;
            前步 = 步;
        }
        //log.info (deque_to_string (mway));
        return 道长;
    }

    private boolean has_path_to (final サラリメン dists,
                                 final int 点, final int 游标) {
        return dists.长里[点][游标] > 0 || dists.长里[游标][点] > 0;
    }

    private int find_shortest_path (final サラリメン dists,
                                    Deque <Integer> mway, final int dist) {
        /**
         * search of shortest path by brute force
         */
        int 远近 = dist, 前步, 步;
        for (int 游标 = 0; 游标 < dists.长里.length; ++游标) { // you biao
            if (!mway.isEmpty()) {
                //log.info (deque2string (mway) + " ?" + 游标);
                if (mway.contains (游标)) continue;
                if (!has_path_to (dists, mway.peekLast(), 游标)) continue;
            }
            mway.offerLast (游标);
            if (mway.size() == dists.长里.length) {
                步 = calculate_distance (dists, mway);
                //if (步 > 0 && __path__ > 步) __path__ = 步;
            }
            else
                步 = find_shortest_path (dists, mway, 远近);
            if (步 > 0 && 远近 > 步) {
                //log.info (远近 + "->" + 步 + "里 :" + deque2string (mway));
                远近 = 步;
            }
            mway.removeLast();
        }
        return 远近;
    }

    private int process1 (final サラリメン dists) {
        /**
         * 1. взять наименьший отрезок p из D;
         * 2. из оставшихся {D\p} и соседних выбрать наименьший q;
         * 3. если {D\(p;q)} не пусто, то п.2;
         */
        Deque <Integer> 大道 = new ArrayDeque <Integer> (dists.长里.length);
        log.info ("expected waypoints "+ dists.长里.length);
        //__path__ = 0x7FFFFF;
        int 远近 = find_shortest_path (dists, 大道, 0x7FFFFF); // yuan jin
        //System.out.println ("__path__ " + __path__);
        return 远近;
    }

    private int process2 (final サラリメン dists) {
        Deque <Integer> 大道 = new ArrayDeque <Integer> (dists.长里.length);
        log.info ("expected waypoints "+ dists.长里.length);
        int 远近 = find_longest_path (dists, 大道, 0); // yuan jin
        return 远近;
    }

    private int find_longest_path (final サラリメン dists,
                                   Deque <Integer> mway, final int dist) {
        /**
         * search of longest path by brute force
         */
        int 远近 = dist, 前步, 步;
        for (int 游标 = 0; 游标 < dists.长里.length; ++游标) { // you biao
            if (!mway.isEmpty()) {
                //log.info (deque2string (mway) + " ?" + 游标);
                if (mway.contains (游标)) continue;
                if (!has_path_to (dists, mway.peekLast(), 游标)) continue;
            }
            mway.offerLast (游标);
            if (mway.size() == dists.长里.length) {
                步 = calculate_distance (dists, mway);
                //if (步 > 0 && __path__ > 步) __path__ = 步;
            }
            else
                步 = find_longest_path (dists, mway, 远近);
            if (步 > 0 && 远近 < 步) {
                //log.info (远近 + "->" + 步 + "里 :" + deque2string (mway));
                远近 = 步;
            }
            mway.removeLast();
        }
        return 远近;
    }

}

class サラリメン {
    final String[] 名字;
    final int[][] 长里;
    final static Pattern pat = Pattern.compile ("^(\\w+) to (\\w+) = (\\d+)$");
    サラリメン(final String[] names, final int[][] dest) {
        名字 = names;
        长里 = dest;
    }

    public void print() {
        for (int 南 = 0; 南 < 长里.length; ++南) {
            System.out.printf ("%d %13s ", 南, 名字[南]);
            for (int 东 = 0; 东 < 长里[南].length; ++东)
                System.out.printf (" %3d", 长里[南][东]);
            System.out.println();
        }
    }
}
