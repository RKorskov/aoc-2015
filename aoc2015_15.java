// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-03 00:03:37 roukoru>

/*
  http://adventofcode.com/2015/day/15

--- Day 15: Science for Hungry People ---

Today, you set out on the task of perfecting your milk-dunking cookie recipe. All you have to do is find the right balance of ingredients.

Your recipe leaves room for exactly 100 teaspoons of ingredients. You make a list of the remaining ingredients you could use to finish the recipe (your puzzle input) and their properties per teaspoon:

    capacity (how well it helps the cookie absorb milk)
    durability (how well it keeps the cookie intact when full of milk)
    flavor (how tasty it makes the cookie)
    texture (how it improves the feel of the cookie)
    calories (how many calories it adds to the cookie)

You can only measure ingredients in whole-teaspoon amounts accurately, and you have to be accurate so you can reproduce your results in the future. The total score of a cookie can be found by adding up each of the properties (negative totals become 0) and then multiplying together everything except calories.

For instance, suppose you have these two ingredients:

Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3

Then, choosing to use 44 teaspoons of butterscotch and 56 teaspoons of cinnamon (because the amounts of each ingredient must add up to 100) would result in a cookie with the following properties:

    A capacity of 44*-1 + 56*2 = 68
    A durability of 44*-2 + 56*3 = 80
    A flavor of 44*6 + 56*-2 = 152
    A texture of 44*3 + 56*-1 = 76

Multiplying these together (68 * 80 * 152 * 76, ignoring calories for now) results in a total score of 62842880, which happens to be the best score possible given these ingredients. If any properties had produced a negative total, it would have instead become zero, causing the whole score to multiply to zero.

Given the ingredients in your kitchen and their properties, what is the total score of the highest-scoring cookie you can make?

*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_15 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;

    final int CAPACITY = 100;

    final String[] test_input = {
        "Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8",
        "Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3"
    };

    final String[] data_input = {
        "Sugar: capacity 3, durability 0, flavor 0, texture -3, calories 2",
        "Sprinkles: capacity -3, durability 3, flavor 0, texture 0, calories 9",
        "Candy: capacity -1, durability 0, flavor 4, texture 0, calories 1",
        "Chocolate: capacity 0, durability 0, flavor -2, texture 2, calories 8"
    };

    public void help() {
        System.out.println
            ("java aoc2015 15 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        //if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
         return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private String[] read_lines (final String src) throws IOException {
        /**
         * read lines from stdin (src="-") or other file
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 7)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[][] read_input (final String src) throws IOException {
        /**
         * read lines from src and converts them to int triplets
         * 1x1x10
         * 2x3x4
         */
        Scanner 输入流 = get_input_scanner (src);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 号.length; ++甲) // && 甲 < 字.length
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin"))
                    串川 = data_input;
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    private InputData parse_input (final String[] 任务) {
        /**
         * Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
         */
        InputData 桂马 = new InputData (); // gui ma
        ArrayList <Ingredient> 食谱 = new ArrayList <Ingredient> (); // shípǔ
        for (String 串 : 任务)
            try {
                食谱.add (new Ingredient (串));
            }
            catch (Exception ex) {
                ; // skip
            }
        桂马.食谱 = 食谱.toArray (new Ingredient [0]);
        return 桂马;
    }

    private int prod (final int[] arr) {
        int 数, 位;
        for (数 = arr[0], 位 = 1; 位 < arr.length; 数 *= arr[位++]);
        return 数;
    }

    private int get_score (final InputData 任务, NPStack np) {
        int[] 比分 = new int [5]; // capacity, durability, flavor, texture, calories
        Arrays.fill (比分, 0);
        int 位, 数;
        for (位 = 0; 位 < np.size(); ++位) {
            数 = np.peek (位);
            比分[0] += 数 * 任务.食谱[位].get_cap();
            比分[1] += 数 * 任务.食谱[位].get_dur();
            比分[2] += 数 * 任务.食谱[位].get_flv();
            比分[3] += 数 * 任务.食谱[位].get_txt();
            比分[4] += 数 * 任务.食谱[位].get_cal();
        }
        return prod (比分);
    }

    private int npsolver (final InputData 任务) {
        return npsolver (任务, new NPStack (任务.食谱.length), 0);
    }

    private int npsolver (final InputData 任务, NPStack np, final int 量) {
        if (量 >= 任务.量)
            return get_score (任务, np);
        int 号;
        if (np.size() == (任务.食谱.length - 1)) {
            // last element
            np.push (任务.量 - 量);
            号 = get_score (任务, np);
            np.pop();
            return 号;
        }
        int 比分 = 0; // bǐfēn
        for (int 位 = 任务.量 - 量; 位 >= 0 ; --位) {
            np.push (位);
            号 = npsolver (任务, np, 量 + 位);
            if (号 > 比分)
                比分 = 号;
            np.pop();
        }
        return 比分;
    }

    private int process1 (final InputData 任务) {
        return npsolver(任务);
    }

    private String process2 (final InputData 任务) {
        return "done!";
    }
}


class InputData {
    Ingredient[] 食谱; // shípǔ
    final int 量 = 100; // total capacity
    InputData() {食谱 = null;}
    // InputData (final String[] 目录) {表 = Arrays.copyOf (目录);}
    public String toString() {return Arrays.toString (食谱);}
}

class Ingredient {
    /**
     * Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
     */
    final private static Pattern 路 = Pattern.compile ("^(?<name>\\w+): capacity (?<cap>-?\\d+), durability (?<dur>-?\\d+), flavor (?<flv>-?\\d+), texture (?<txt>-?\\d+), calories (?<cal>-?\\d+)$"); // lù
    final String 名字;
    final int 卡, // kǎ
        量, // liàng
        耐力, // nàilì
        味, // wèi
        理; // lǐ

    Ingredient (final String 串) throws Exception{
        Matcher 配 = 路.matcher (串);
        if (配 == null || !配.matches())
            throw new Exception ("bad string : " + 串);
        名字 = 配.group ("name");
        量 = Integer.parseInt (配.group ("cap"));
        耐力 = Integer.parseInt (配.group ("dur"));
        味 = Integer.parseInt (配.group ("flv"));
        理 = Integer.parseInt (配.group ("txt"));
        卡 = Integer.parseInt (配.group ("cal"));
    }

    public int get_cap() {return 量;}
    public int get_dur() {return 耐力;}
    public int get_flv() {return 味;}
    public int get_txt() {return 理;}
    public int get_cal() {return 卡;}

    public String toString() {return null;}
}


