// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-17 23:06:02 roukoru>

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

interface SEGate {
    //public static SEGate construct (String op);
    public int join_input (SEGate in);
    public int join_output (SEGate out);
    public int get_output_value();
    public SEGate get_output();
    public SEGate[] get_input();
    public boolean hasOutput(); // true, if all inputs properly connected
}
