// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-01 22:46:27 roukoru>

/*
  http://adventofcode.com/2015/day/13

--- Day 13: Knights of the Dinner Table ---

In years past, the holiday feast with your family hasn't gone so well. Not everyone gets along! This year, you resolve, will be different. You're going to find the optimal seating arrangement and avoid all those awkward conversations.

You start by writing up a list of everyone invited and the amount their happiness would increase or decrease if they were to find themselves sitting next to each other person. You have a circular table that will be just big enough to fit everyone comfortably, and so each person will have exactly two neighbors.

For example, suppose you have only four attendees planned, and you calculate their potential happiness as follows:

Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.

Then, if you seat Alice next to David, Alice would lose 2 happiness units (because David talks so much), but David would gain 46 happiness units (because Alice is such a good listener), for a total change of 44.

If you continue around the table, you could then seat Bob next to Alice (Bob gains 83, Alice gains 54). Finally, seat Carol, who sits next to Bob (Carol gains 60, Bob loses 7) and David (Carol gains 55, David gains 41). The arrangement looks like this:

     +41 +46
+55   David    -2
Carol       Alice
+60    Bob    +54
     -7  +83

After trying every other seating arrangement in this hypothetical scenario, you find that this one is the most optimal, with a total change in happiness of 330.

What is the total change in happiness for the optimal seating arrangement of the actual guest list?

--- Part Two ---

In all the commotion, you realize that you forgot to seat yourself. At this point, you're pretty apathetic toward the whole thing, and your happiness wouldn't really go up or down regardless of who you sit next to. You assume everyone else would be just as ambivalent about sitting next to you, too.

So, add yourself to the list, and give all happiness relationships that involve you a score of 0.

What is the total change in happiness for the optimal seating arrangement that actually includes yourself?

*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.NoSuchElementException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_13 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    String[] test_input = {
        "Alice would gain 54 happiness units by sitting next to Bob.",
        "Alice would lose 79 happiness units by sitting next to Carol.",
        "Alice would lose 2 happiness units by sitting next to David.",
        "Bob would gain 83 happiness units by sitting next to Alice.",
        "Bob would lose 7 happiness units by sitting next to Carol.",
        "Bob would lose 63 happiness units by sitting next to David.",
        "Carol would lose 62 happiness units by sitting next to Alice.",
        "Carol would gain 60 happiness units by sitting next to Bob.",
        "Carol would gain 55 happiness units by sitting next to David.",
        "David would gain 46 happiness units by sitting next to Alice.",
        "David would lose 7 happiness units by sitting next to Bob.",
        "David would gain 41 happiness units by sitting next to Carol."
    };

    public void help() {;}

    public void eval (final String[] args) throws IOException { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        try {
            if (args.length < 1) return;
            //int[][] 任务 = read_input (args[1]);
            InputData 任务 = read_parse_input (args); // rènwù
            // String 任务 = args[1].equals ("-") ? task_input : args[1];
            System.out.println (process1 (任务));
            任务.expand();
            任务.print();
            System.out.println (process1 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
         return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private String[] read_lines (final String src) throws IOException {
        /**
         * read lines from stdin (src="-") or other file
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 7)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[][] read_input (final String src) throws IOException {
        /**
         * read lines from src and converts them to int triplets
         * 1x1x10
         * 2x3x4
         */
        Scanner 输入流 = get_input_scanner (src);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 号.length; ++甲) // && 甲 < 字.length
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else
            串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        return parse_input (串川);
    }

    private InputData parse_input (final String[] indata) {
        /**
         * Alice would lose 2 happiness units by sitting next to David.
         * Bob would gain 83 happiness units by sitting next to Alice.
         */
        InputData 桂马 = new InputData (); // gui ma
        ArrayList <Edge> 边沿 = new ArrayList <Edge> (); // biānyán
        Map <String, Integer> 名字 = new HashMap <String, Integer> ();
        parse_relations (indata, 名字, 边沿);
        桂马.名字 = build_name_list (名字);
        桂马.连系 = 边沿.toArray(new Edge[0]);
        return 桂马;
    }

    private void parse_relations (final String[] indata,
                                  Map <String, Integer> 名字,
                                  ArrayList <Edge> edges) {
        final Pattern pat13 = Pattern.compile
            ("^(?<dst>\\w+) would (?<sign>(?:gain)|(?:lose)) (?<weight>\\d+) happiness units by sitting next to (?<src>\\w+).$");
        for (String 串 : indata) {
            Matcher 配 = pat13.matcher (串); // pèi
            if (!配.matches()) continue;
            String 者 = 配.group ("dst"), // zhe
                客 = 配.group ("src"); // ke
            update_names (名字, 者, 客); // ??
            int 者号 = 名字.get (者),
                客号 = 名字.get (客);
            int 重 = Integer.parseInt (配.group ("weight"));
            if (配.group("sign").equals ("lose"))
                重 = -重;
            edges.add (new Edge (者号, 客号, 重));
        }
    }

    private String[] build_name_list (final Map <String, Integer> names) {
        String[] 名字表 = new String [names.size()];
        for (String 名字 : names.keySet())
            名字表[names.get (名字)] = 名字;
        return 名字表;
    }

    private int update_names (Map <String, Integer> 名字,
                              final String dst, final String src) {
        int 号 = 名字.size();
        if (!名字.containsKey (dst))
            名字.put (dst, 号++);
        if (!名字.containsKey (src))
            名字.put (src, 号++);
        return 号;
    }

    private int calculate_happiness (final InputData 任务,
                                     Table 桌子) {
        int 辛 = 0, 客, 左, 右;
        for (int 位 = 0; 位 < 任务.名字.length; ++位) { // iterator, really...
            客 = 桌子.peek(位);
            右 = 桌子.next(位);
            左 = 桌子.prev(位);
            辛 += 任务.get (客, 右) + 任务.get (客, 左);
        }
        // System.out.printf ("%d ", 辛);
        return 辛;
    }

    private int populate (final InputData 任务) {
        Table 桌子 = new Table (任务.名字.length); // zhuo1zi
        return populate (任务, 桌子);
    }

    private int populate (final InputData 任务,
                          Table 桌子) {
        if (桌子.size() == 任务.名字.length)
            return calculate_happiness (任务, 桌子);
        int 客, 辛 = -0x7FFFFF; // ke xin
        for (客 = 0; 客 < 任务.名字.length; ++客) {
            if (桌子.contains (客)) continue;
            桌子.push (客);
            int 甲 = populate (任务, 桌子);
            if (甲 > 辛)
                辛 = 甲;
            桌子.pop();
        }
        return 辛;
    }

    private int process1 (final InputData 任务) {
        return populate (任务);
    }

}


class InputData {
    String[] 名字; // míng zì
    Edge[] 连系; // liánxì
    InputData() {
        名字 = null;
        连系 = null;
    }

    public int get (final int 谁, final int 去) throws NoSuchElementException {
        for (Edge 边 : 连系) // biān
            if (边.谁 == 谁 && 边.去 == 去)
                return 边.重;
        throw new NoSuchElementException
            (String.format ("edge not exist %d to %d", 谁, 去));
    }

    public void print () {
        for (Edge 边 : 连系)
            System.out.printf ("%s <- %s : %d\n",
                               名字[边.谁], 名字[边.去], 边.重);
    }

    public void expand() {
        /**
         * adds set of zero-weight edges to everyone
         * and a new name, too
         */
        int 东 = 名字.length, 家 = 连系.length;
        名字 = Arrays.copyOf (名字, 名字.length + 1);
        名字[东] = "AoC2015";
        连系 = Arrays.copyOf (连系, 连系.length + 东*2);
        for (int 客 = 0; 客 < 东; ++客) {
            连系[家++] = new Edge (客, 东, 0);
            连系[家++] = new Edge (东, 客, 0);
        }
    }
}


class Edge {
    final int 重, // zhòng
        谁, 去; // shei

    Edge() {
        重 = 0;
        谁 = 去 = -1;
    }

    Edge (final int dst, final int src, final int wgt) {
        重 = wgt;
        谁 = dst;
        去 = src;
    }

    public String toString() {
        return String.format ("(%d : %d) %d", 谁, 去, 重);
    }
}


class Table {
    int[] 目录; // mù lù
    int 量; // liàng

    Table() {目录 = null;}

    Table (final int guests) {
        this.目录 = new int [guests];
        量 = 0;
        Arrays.fill (目录, -1);
    }

    public void clear() {量=0;Arrays.fill (目录, -1);}
    public int size() {return 量;}
    public String toString() {return Arrays.toString (目录);}

    public void push (final int 客) throws NoSuchElementException {
        if (量 < 0) 量 = 0;
        if (量 < 目录.length)
            目录[量++] = 客;
        else
            throw new NoSuchElementException ("no more seats");
    }

    public int pop() throws NoSuchElementException {
        if (量 < 1 || 量 > 目录.length)
            throw new NoSuchElementException ("no guests");
        return 目录[--量];
    }

    public int peek() throws NoSuchElementException {
        if (量 < 1 || 量 > 目录.length)
            throw new NoSuchElementException ("no guests");
        return 目录[量-1];
    }

    public int peek (final int 位) throws NoSuchElementException {
        if (位 < 0 || 位 >= 量)
            throw new NoSuchElementException ("no this guest");
        return 目录[位];
    }

    public int prev (final int 位) throws NoSuchElementException {
        if (位 < 0 || 位 >= 量)
            throw new NoSuchElementException ("no this guest");
        int 甲 = 位 - 1;
        if (甲 < 0) 甲 = 量 - 1;
        return 目录[甲];
    }

    public int next (final int 位) throws NoSuchElementException {
        if (位 < 0 || 位 >= 量)
            throw new NoSuchElementException ("no this guest");
        int 甲 = 位 + 1;
        if (甲 >= 量) 甲 = 0;
        return 目录[甲];
    }

    public boolean contains (final int 客) {
        for (int 位 = 0; 位 < 量; ++位)
            if (客 == 目录[位])
                return true;
        return false;
    }
}
