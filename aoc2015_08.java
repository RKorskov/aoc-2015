// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-18 13:18:45 roukoru>

/*
  http://adventofcode.com/2015/day/8

--- Day 8: Matchsticks ---

Space on the sleigh is limited this year, and so Santa will be bringing his list as a digital copy. He needs to know how much space it will take up when stored.

It is common in many programming languages to provide a way to escape special characters in strings. For example, C, JavaScript, Perl, Python, and even PHP handle special characters in very similar ways.

However, it is important to realize the difference between the number of characters in the code representation of the string literal and the number of characters in the in-memory string itself.

For example:

    "" is 2 characters of code (the two double quotes), but the string contains zero characters.
    "abc" is 5 characters of code, but 3 characters in the string data.
    "aaa\"aaa" is 10 characters of code, but the string itself contains six "a" characters and a single, escaped quote character, for a total of 7 characters in the string data.
    "\x27" is 6 characters of code, but the string itself contains just one - an apostrophe ('), escaped using hexadecimal notation.

Santa's list is a file that contains many double-quoted string literals, one on each line. The only escape sequences used are \\ (which represents a single backslash), \" (which represents a lone double-quote character), and \x plus two hexadecimal characters (which represents a single character with that ASCII code).

Disregarding the whitespace in the file, what is the number of characters of code for string literals minus the number of characters in memory for the values of the strings in total for the entire file?

For example, given the four strings above, the total number of characters of string code (2 + 5 + 10 + 6 = 23) minus the total number of characters in memory for string values (0 + 3 + 7 + 1 = 11) is 23 - 11 = 12.

--- Part Two ---

Now, let's go the other way. In addition to finding the number of characters of code, you should now encode each code representation as a new string and find the number of characters of the new encoded representation, including the surrounding double quotes.

For example:

    "" encodes to "\"\"", an increase from 2 characters to 6.
    "abc" encodes to "\"abc\"", an increase from 5 characters to 9.
    "aaa\"aaa" encodes to "\"aaa\\\"aaa\"", an increase from 10 characters to 16.
    "\x27" encodes to "\"\\x27\"", an increase from 6 characters to 11.

Your task is to find the total number of characters to represent the newly encoded strings minus the number of characters of code in each original string literal. For example, for the strings above, the total encoded length (6 + 9 + 16 + 11 = 42) minus the characters in the original code representation (23, just like in the first part of this puzzle) is 42 - 23 = 19.

*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.LinkedList;
// import java.util.List;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.TreeMap;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Logger;

public class aoc2015_08 implements AoC {

    final boolean __USE_LOG = false;
    final String task_description = "what shall I do?";
    final String task_input = "... with this?";
    final String[] test_input = {"test_1", "test_2"};

    public void eval (final String[] args) {
        if (args.length < 2) {help(); return;}
        String[] tis = read_input ("-");
        //String tis = args[1].compareTo ("-") == 0 ? task_input : args[1];
        System.out.println (process1 (tis));
        System.out.println (process2 (tis));
    }

    public void help() {
        print_task();
        print_input_1();
    }

    public void print_task() {
        /** print task(s) **/
        System.out.println (task_description);
    }
    
    public void print_input_test() {
        // "test" (if any) input task
        System.out.println (test_input);
    }

    public void print_output_test() {
        // "test" (if any) output task
        System.out.println ("test output");
    }

    public void print_input_1() {
        // "basic" input task
        System.out.println (task_input);
    }

    public void print_input_2() {
        // "extended" (if any) input task
        System.out.println ("task input №2");
    }

    private String[] read_input (final String src) {
        // 1x1x10
        // 2x3x4
        Scanner sc;
        // if (src == "-") ssrc = System.in; else ssrc = ...;
        sc = new Scanner (System.in);
        ArrayList <String> 号号 = new ArrayList <> ();
        for (;sc.hasNext();) {
            String 串 = sc.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 2) break;
            号号.add (串);
        }
        return 号号.toArray (new String[号号.size()]);
    }

    private int strlen (final String str) {
        // string is double-quoted, i.e.: "...string..."$
        int 长 = 0; // chang
        for (int 位 = 1; 位 < (str.length() - 1); ++位) {
            if (str.charAt (位) == '\\') {
                switch (str.charAt (++位)) {
                case '\\': break;
                case '"': break;
                case 'x': {位 += 2; break;}
                default: --位;
                }
            }
            ++长;
        }
        return 长;
    }

    private int encode_len (final String str) {
        // string is double-quoted, i.e.: "...string..."$
        int 长 = 2; // chang
        for (int 位 = 0; 位 < str.length(); ++位) {
            switch (str.charAt (位)) {
            case '\\': {++长; break;}
            case '"': {++长; break;}
            }
            ++长;
        }
        return 长;
    }

    private int process1 (final String[] task_input) {
        int rv = 0;
        for (String 串 : task_input)
            rv += 串.length() - strlen (串);
        return rv;
    }

    private int process2 (final String[] task_input) {
        int rv = 0;
        for (String 串 : task_input)
            rv += encode_len (串) - 串.length();
        return rv;
    }

}
