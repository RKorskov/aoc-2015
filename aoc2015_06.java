// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-15 00:08:05 roukoru>

/*
  http://adventofcode.com/2015/day/6
--- Day 6: Probably a Fire Hazard ---

Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.

For example:

    turn on 0,0 through 999,999 would turn on (or leave on) every light.
    toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
    turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.

After following the instructions, how many lights are lit?

--- Part Two ---

You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.

The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.

The phrase turn on actually means that you should increase the brightness of those lights by 1.

The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.

The phrase toggle actually means that you should increase the brightness of those lights by 2.

What is the total brightness of all lights combined after following Santa's instructions?

For example:

    turn on 0,0 through 0,0 would increase the total brightness by 1.
    toggle 0,0 through 999,999 would increase the total brightness by 2000000.
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.security.MessageDigest;

// import java.text.DateFormat;

import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
import java.util.Scanner;
// import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;

import java.util.logging.Logger;

public class aoc2015_06 implements AoC {

    final boolean __USE_LOG = false;
    final String task_description = "what shall I do?";
    final String task_input = "... with this?";
    final String[] test_input = {"test_1", "test_2"};

    public void eval (final String[] args) {
        if (args.length < 2) {help(); return;}
        SLight[] tis = read_input ("-");
        System.out.println (tis.length);
        System.out.println (process1 (tis));
        System.out.println (process2 (tis));
    }

    public void help() {
        print_task();
        print_input_1();
    }

    public void print_task() {
        /** print task(s) **/
        System.out.println (task_description);
    }
    
    public void print_input_test() {
        // "test" (if any) input task
        System.out.println (test_input);
    }

    public void print_output_test() {
        // "test" (if any) output task
        System.out.println ("test output");
    }

    public void print_input_1() {
        // "basic" input task
        System.out.println (task_input);
    }

    public void print_input_2() {
        // "extended" (if any) input task
        System.out.println ("task input №2");
    }

    private SLight[] read_input (final String src) {
        Scanner sc;
        // if (src == "-") ssrc = System.in; else ssrc = ...;
        sc = new Scanner (System.in);
        ArrayList <SLight> 号号 = new ArrayList <> ();
        for (;sc.hasNext();) {
            String 串 = sc.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            // turn on 1,1 through 2,2
            if (串 == null || 串.length() < 23) break;
            SLight 灯 = SLight.newSLight (串); // deng
            if (灯 != null) 号号.add (灯);
        }
        return 号号.toArray (new SLight[号号.size()]);
    }

    private int process1 (final SLight[] task_input) {
        SGrid 表 = new SGrid (1000, 1000);
        for (SLight 灯 : task_input)
            表.apply (灯);
        return 表.count_lighted();
    }

    private int process2 (final SLight[] task_input) {
        ASGrid 表 = new ASGrid (1000, 1000);
        for (SLight 灯 : task_input)
            表.apply (灯);
        return 表.count_lighted();
    }

}


enum SAction {ON, OFF, TOGGLE;}

class SLight {
    SAction action;
    int fromX, fromY,
        toX, toY;

    /**
     * turn on 0,0 through 999,999 would turn on (or leave on) every light.
     * toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
     * turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.
     **/

    private static Pattern spat = Pattern.compile
        ("^(?<act>(turn on)|(turn off)|(toggle))\\s+(?<fx>\\d+),(?<fy>\\d+)\\s+through\\s+(?<tx>\\d+),(?<ty>\\d+)$");

    private SLight (final SAction act,
                    final int fx, final int fy, final int tx, final int ty) {
        fromX = fx;
        fromY = fy;
        toX = tx;
        toY = ty;
        action = act;
    }

    public static SLight newSLight (final String src) {
        Matcher m = spat.matcher (src);
        if (m == null || !m.matches()) return null;
        SAction a = SAction.TOGGLE;
        switch (m.group ("act")) {
        case "turn on": {a = SAction.ON; break;}
        case "turn off": {a = SAction.OFF; break;}
            //default: {a = SAction.TOGGLE; break;}
        }
        return new SLight (a, Integer.parseInt (m.group ("fx")),
                           Integer.parseInt (m.group ("fy")),
                           Integer.parseInt (m.group ("tx")),
                           Integer.parseInt (m.group ("ty")));
    }
}


class SGrid {
    boolean[][] grid;

    SGrid (final int length, final int height) {
        grid = new boolean [height][];
        for (int 行 = 0; 行 < height; ++行) // hang/xing
            grid[行] = new boolean [length];
    }

    public void apply (SLight action) {
        for (int 行 = action.fromY; 行 <= action.toY; ++行) {
            for (int 列 = action.fromX; 列 <= action.toX; ++列) { // lie
                switch (action.action) {
                case ON: {grid[行][列]=true; break;}
                case OFF: {grid[行][列]=false; break;}
                case TOGGLE: {grid[行][列]=!grid[行][列]; break;}
                }
            }
        }
    }

    public int count_lighted() {
        int 号 = 0;
        for (int 行 = 0; 行 < grid.length; ++行)
            for (int 列 = 0; 列 < grid[行].length; ++列)
                if (grid[行][列]) ++号;
        return 号;
    }
}


class ASGrid {
    int[][] grid;

    ASGrid (final int length, final int height) {
        grid = new int [height][];
        for (int 行 = 0; 行 < height; ++行) // hang/xing
            grid[行] = new int [length];
    }

    public void apply (SLight action) {
        for (int 行 = action.fromY; 行 <= action.toY; ++行) {
            for (int 列 = action.fromX; 列 <= action.toX; ++列) { // lie
                switch (action.action) {
                case ON: {++grid[行][列]; break;}
                case OFF: {if (grid[行][列] > 0) --grid[行][列]; break;}
                case TOGGLE: {grid[行][列] += 2; break;}
                }
            }
        }
    }

    public int count_lighted() {
        int 号 = 0;
        for (int 行 = 0; 行 < grid.length; ++行)
            for (int 列 = 0; 列 < grid[行].length; ++列)
                if (grid[行][列] > 0) 号 += grid[行][列];
        return 号;
    }
}
