// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-24 16:30:19 roukoru>

/*
  http://adventofcode.com/2015/day/12

--- Day 12: JSAbacusFramework.io ---

Santa's Accounting-Elves need help balancing the books after a recent order. Unfortunately, their accounting software uses a peculiar storage format. That's where you come in.

They have a JSON document which contains a variety of things: arrays ([1,2,3]), objects ({"a":1, "b":2}), numbers, and strings. Your first job is to simply find all of the numbers throughout the document and add them together.

For example:

    [1,2,3] and {"a":2,"b":4} both have a sum of 6.
    [[[3]]] and {"a":{"b":4},"c":-1} both have a sum of 3.
    {"a":[-1,1]} and [-1,{"a":1}] both have a sum of 0.
    [] and {} both have a sum of 0.

You will not encounter any strings containing numbers.

What is the sum of all numbers in the document?

--- Part Two ---

Uh oh - the Accounting-Elves have realized that they double-counted everything red.

Ignore any object (and all of its children) which has any property with the value "red". Do this only for objects ({...}), not arrays ([...]).

    [1,2,3] still has a sum of 6.
    [1,{"c":"red","b":2},3] now has a sum of 4, because the middle object is ignored.
    {"d":"red","e":[1,2,3,4],"f":5} now has a sum of 0, because the entire structure is ignored.
    [1,"red",5] has a sum of 6, because "red" in an array has no effect.
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.LinkedList;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.TreeMap;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_12 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;

    public void help() {;}

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        if (args.length < 1) return;
        String tis;
        if (args.length < 2 || args[1].equals ("-"))
            tis = read_lines("-")[0];
        else
            tis = args[1];
        System.out.println (process1 (tis));
        System.out.println (process2 (tis));
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
         return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private String[] read_lines (final String src) throws IOException {
        /**
         * read lines from stdin (src="-") or other file
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 7)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[][] read_input (final String src) throws IOException {
        /**
         * read lines from src and converts them to int triplets
         * 1x1x10
         * 2x3x4
         */
        Scanner 输入流 = get_input_scanner (src);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 号.length; ++甲) // && 甲 < 字.length
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    Pattern npat = Pattern.compile ("([-]?\\d+)");
    private long process1 (final String task_input) {
        long rl;
        Matcher nm = npat.matcher (task_input);
        for (rl = 0; nm.find();) {
            int n = Integer.parseInt (nm.group(1));
            rl += n;
        }
        return rl;
    }

    private String process2 (final String task_input) {
        return "done!";
    }
}
