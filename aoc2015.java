// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-02 21:18:41 roukoru>

/*
  http://adventofcode.com/2015
*/

// package aoc2015;

import java.lang.reflect.Constructor;
import java.lang.Class;

//import java.io.Console; // System.console ();
// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
// import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Scanner; // Scanner (System.in);
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;


public class aoc2015 {

    public static boolean __DEBUG__ = false;

    public static void main (String[] args) throws Exception {
        //(new aoc2015()).reflector (args);
        (new aoc2015()).selector (args);
    }

    private void reflector (final String[] args) {
        // так делать нинада...
        String daily_class = "aoc2015_" + args[0];
        try {
            Class <?> aoc_cls = Class.forName (daily_class);
            Constructor <?> aoc_cns = aoc_cls.getConstructor();
            AoC aoc_o = (AoC) aoc_cns.newInstance();
            aoc_o.eval (args);
        }

        catch (Exception ex) {
            System.out.println ("... oops! with " + daily_class);
        }
        return;
    }


    private void selector (final String[] args) {
        try {
            int day = Integer.parseInt (args[0]);
            switch (day) {
            case 1: {(new aoc2015_01()).eval (args); break;}
            case 2: {(new aoc2015_02()).eval (args); break;}
            case 3: {(new aoc2015_03()).eval (args); break;}
            case 4: {(new aoc2015_04()).eval (args); break;}
            case 5: {(new aoc2015_05()).eval (args); break;}
            case 6: {(new aoc2015_06()).eval (args); break;}
            case 7: {(new aoc2015_07()).eval (args); break;}
            case 8: {(new aoc2015_08()).eval (args); break;}
            case 9: {(new aoc2015_09()).eval (args); break;}
            case 10: {(new aoc2015_10()).eval (args); break;}
            case 11: {(new aoc2015_11()).eval (args); break;}
            case 12: {(new aoc2015_12()).eval (args); break;}
            case 13: {(new aoc2015_13()).eval (args); break;}
            case 14: {(new aoc2015_14()).eval (args); break;}
            case 15: {(new aoc2015_15()).eval (args); break;}
            // case 16: {(new aoc2015_16()).eval (args); break;}
            // case 17: {(new aoc2015_17()).eval (args); break;}
            // case 18: {(new aoc2015_18()).eval (args); break;}
            // case 19: {(new aoc2015_19()).eval (args); break;}
            // case 20: {(new aoc2015_20()).eval (args); break;}
            // case 21: {(new aoc2015_21()).eval (args); break;}
            // case 22: {(new aoc2015_22()).eval (args); break;}
            // case 23: {(new aoc2015_23()).eval (args); break;}
            // case 24: {(new aoc2015_24()).eval (args); break;}
            // case 25: {(new aoc2015_25()).eval (args); break;}
            default: System.out.println ("java aoc2015 day_number someargs");
            }
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

