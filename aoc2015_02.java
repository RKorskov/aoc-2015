// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-13 23:56:21 roukoru>

/*
  http://adventofcode.com/2015/day/2

--- Day 2: I Was Told There Would Be No Math ---

The elves are running low on wrapping paper, and so they need to submit an order for more. They have a list of the dimensions (length l, width w, and height h) of each present, and only want to order exactly as much as they need.

Fortunately, every present is a box (a perfect right rectangular prism), which makes calculating the required wrapping paper for each gift a little easier: find the surface area of the box, which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the area of the smallest side.

For example:

    A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
    A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.

All numbers in the elves' list are in feet. How many total square feet of wrapping paper should they order?

--- Part Two ---

The elves are also running low on ribbon. Ribbon is all the same width, so they only have to worry about the length they need to order, which they would again like to be exact.

The ribbon required to wrap a present is the shortest distance around its sides, or the smallest perimeter of any one face. Each present also requires a bow made out of ribbon as well; the feet of ribbon required for the perfect bow is equal to the cubic feet of volume of the present. Don't ask how they tie the bow, though; they'll never tell.

For example:

    A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
    A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.

How many total feet of ribbon should they order?
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
import java.util.Scanner;
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;

import java.util.logging.Logger;

public class aoc2015_02 implements AoC {

    final boolean __USE_LOG = false;
    final String task_description = "what shall I do?";
    final String task_input_1 = "... with this?";

    public void eval (final String[] args) {
        if (args.length < 2) {help(); return;}
        int[][] box_list = read_input ("-");
        System.out.println (process_paper (box_list));
        System.out.println (process_ribbon (box_list));
    }

    public void help() {
        print_task();
        print_input_1();
    }

    public void print_task() {
        /** print task(s) **/
        System.out.println (task_description);
    }
    
    public void print_input_test() {
        // "test" (if any) input task
        System.out.println ("test input");
    }

    public void print_output_test() {
        // "test" (if any) output task
        System.out.println ("test output");
    }

    public void print_input_1() {
        // "basic" input task
        System.out.println (task_input_1);
    }

    public void print_input_2() {
        // "extended" (if any) input task
        System.out.println ("task input №2");
    }

    private int[][] read_input (final String src) {
        // 1x1x10
        // 2x3x4
        Scanner sc;
        // if (src == "-") ssrc = System.in; else ssrc = ...;
        sc = new Scanner (System.in);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (;sc.hasNext();) {
            String 串 = sc.nextLine();
            if (__USE_LOG == true) {
                Logger log = Logger.getGlobal();
                log.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 3; ++甲)
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    private int eval_box_area (final int[] box_size) {
        return 2 * (box_size[0] * box_size[1] +
                    box_size[0] * box_size[2] +
                    box_size[1] * box_size[2]);
    }

    private int get_box_smallest_side (final int[] box_size) {
        int s0 = box_size[0] * box_size[1],
            s1 = box_size[0] * box_size[2],
            s2 = box_size[1] * box_size[2];
        return min (s0, s1, s2);
    }

    private int eval_perimeter (final int a, final int b) {
        return 2 * (a + b);
    }

    private int eval_box_volume (final int[] box_size) {
        return box_size[0] * box_size[1] * box_size[2];
    }

    private int get_box_smallest_perimeter (final int[] box_size) {
        int p0 = eval_perimeter (box_size[0], box_size[1]),
            p1 = eval_perimeter (box_size[0], box_size[2]),
            p2 = eval_perimeter (box_size[1], box_size[2]);
        return min (p0, p1, p2);
    }

    private int min (final int s0, final int s1, final int s2) {
        int 甲 = s0<s1?s0:s1;
        return s2<甲?s2:甲;
    }

    private int process_paper (final int[][] box_list) {
        // paper
        int res = 0;
        for (int 甲 = 0;甲 < box_list.length;++甲) {
            res += eval_box_area (box_list[甲]) +
            get_box_smallest_side (box_list[甲]);
        }
        return res;
    }

    private int process_ribbon (final int[][] box_list) {
        // ribbon
        int res = 0;
        for (int 甲 = 0;甲 < box_list.length;++甲) {
            res += eval_box_volume (box_list[甲]) +
            get_box_smallest_perimeter (box_list[甲]);
        }
        return res;
    }
}
