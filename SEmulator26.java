// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-17 23:05:01 roukoru>

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;


class SEmulator26 implements SEmulator {
    private final int Number_of_Wires = 26;
    int[] wires; // a..z
    int ip;
    boolean[] active;

    SEmulator26() {
        wires = new int [Number_of_Wires];
        active = new boolean [Number_of_Wires];
        ip = 0;
    }

    public boolean emulate (final String[] ops) {
        for (String op : ops)
            if (gate_set (op) ||
                gate_and (op) ||
                gate_or (op)  ||
                gate_shl (op) ||
                gate_shr (op) ||
                gate_not (op)) {++ip; continue;}
            else return false;
        return true;
    }

    public void print() {
        for (int i = 0; i < wires.length; ++i)
            if (active[i])
                System.out.printf ("%02d : %d\n", i, wires[i]);
    }

    public int get_ip() {return ip;}

    public int get_wire (final String widx) {
        int w = get_wire_index (widx);
        if (w < 0 || w >= Number_of_Wires) return -1;
        return wires[w];
    }

    private int get_wire_index (final String swid) {
        return swid.charAt(0) - 'a';
    }

    private boolean gate_set (final String op) {
        Matcher m = SEmulator.pat_gate_buf.matcher (op);
        if (m == null || m.matches() == false) return false;
        int n = Integer.parseInt (m.group ("value")),
            wo = get_wire_index (m.group("out"));
        wires[wo] = n;
        active[wo] = true;
        return true;
    }

    private boolean gate_not (final String op) {
        Matcher m = SEmulator.pat_gate_not.matcher (op);
        if (m == null || m.matches() == false) return false;
        int wi = get_wire_index (m.group("in")),
            wo = get_wire_index (m.group("out"));
        wires[wo] = ~wires[wi];
        wires[wo] &= MASK;
        active[wo] = true;
        return true;
    }

    private boolean gate_shl (final String op) {
        Matcher m = SEmulator.pat_gate_shl.matcher (op);
        if (m == null || m.matches() == false) return false;
        int cl = Integer.parseInt (m.group ("rep")),
            wi = get_wire_index (m.group("in")),
            wo = get_wire_index (m.group("out"));
        wires[wo] = wires[wi] << cl;
        wires[wo] &= MASK;
        active[wo] = true;
        return true;
    }

    private boolean gate_shr (final String op) {
        Matcher m = SEmulator.pat_gate_shr.matcher (op);
        if (m == null || m.matches() == false) return false;
        int cl = Integer.parseInt (m.group ("rep")),
            wi = get_wire_index (m.group("in")),
            wo = get_wire_index (m.group("out"));
        wires[wo] &= MASK;
        wires[wo] = wires[wi] >> cl;
        active[wo] = true;
        return true;
    }

    private boolean gate_and (final String op) {
        Matcher m = SEmulator.pat_gate_and.matcher (op);
        if (m == null || m.matches() == false) return false;
        int wi0 = get_wire_index (m.group("in0")),
            wi1 = get_wire_index (m.group("in1")),
            wo = get_wire_index (m.group("out"));
        wires[wo] = wires[wi0] & wires[wi1];
        wires[wo] &= MASK;
        active[wo] = true;
        return true;
    }

    private boolean gate_or (final String op) {
        Matcher m = SEmulator.pat_gate_or.matcher (op);
        if (m == null || m.matches() == false) return false;
        int wi0 = get_wire_index (m.group("in0")),
            wi1 = get_wire_index (m.group("in1")),
            wo = get_wire_index (m.group("out"));
        wires[wo] = wires[wi0] | wires[wi1];
        wires[wo] &= MASK;
        active[wo] = true;
        return true;
    }

}
