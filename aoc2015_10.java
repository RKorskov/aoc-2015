// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-23 00:02:53 roukoru>

/*
  http://adventofcode.com/2015/day/10

--- Day 10: Elves Look, Elves Say ---

Today, the Elves are playing a game called look-and-say. They take turns making sequences by reading aloud the previous sequence and using that reading as the next sequence. For example, 211 is read as "one two, two ones", which becomes 1221 (1 2, 2 1s).

Look-and-say sequences are generated iteratively, using the previous value as input for the next step. For each step, take the previous value, and replace each run of digits (like 111) with the number of digits (3) followed by the digit itself (1).

For example:

    1 becomes 11 (1 copy of digit 1).
    11 becomes 21 (2 copies of digit 1).
    21 becomes 1211 (one 2 followed by one 1).
    1211 becomes 111221 (one 1, one 2, and two 1s).
    111221 becomes 312211 (three 1s, two 2s, and one 1).

Starting with the digits in your puzzle input, apply this process 40 times. What is the length of the result?

Your puzzle input is 3113322113

--- Part Two ---

Neat, right? You might also enjoy hearing John Conway talking about this sequence (that's Conway of Conway's Game of Life fame).

https://www.youtube.com/watch?v=ea7lJkEhytA

Now, starting again with the digits in your puzzle input, apply this process 50 times. What is the length of the new result?
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
import java.lang.StringBuilder;

// import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.LinkedList;
// import java.util.List;
// import java.util.Map;
// import java.util.Scanner;
// import java.util.Set;
// import java.util.TreeMap;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_10 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String task_input = "3113322113";

    public void help() {;}

    public void eval (final String[] args) { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        if (args.length < 1) return;
        try {
        String tis = args[1].equals ("-") ? task_input : args[1];
        int[] aoi = string2ints (tis);
        System.out.println (process (aoi, 40));
        System.out.println (process (aoi, 50));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private int[] string2ints (final String src) {
        int[] 矩阵 = new int [src.length()]; // ju zhen
        for (int 位 = 0; 位 < src.length(); ++位)
            矩阵[位] = src.charAt(位) - '0';
        return 矩阵;
    }

    private String ints2string (final int[] src) {
        StringBuilder 串 = new StringBuilder (src.length);
        for (int 号 : src)
            串.append (String.valueOf (号));
        return 串.toString();
    }

    private int get_plato_length (final int[] src, final int pos) {
        int 位 = pos + 1,
            号 = src[pos];
        for (; 位 < src.length && 号 == src[位]; ++位) ;
        return 位 - pos;
    }

    private int[] look_and_say (final int[] src) {
        ArrayList <Integer> 矩阵 = new ArrayList <Integer> ();
        int 长;
        for (int 位 = 0; 位 < src.length;) {
            长 = get_plato_length (src, 位);
            矩阵.add (长);
            矩阵.add (src[位]);
            if (长 < 1) return null;
            位 += 长;
        }
        int[] 向量 = new int[矩阵.size()]; // xiàngliàng
        for (int 位 = 0; 位 < 矩阵.size(); ++位)
            向量[位] = 矩阵.get(位);
        //return 矩阵.toArray (new int[矩阵.size()]);
        return 向量;
    }

    private int process (final int[] task_input, final int repetitions) {
        int[] 矩阵 = task_input;
        for (int 甲 = 0; 甲 < repetitions; ++甲)
            矩阵 = look_and_say (矩阵);
        return 矩阵.length;
    }

}
