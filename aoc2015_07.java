// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-18 10:16:11 roukoru>

/*
  http://adventofcode.com/2015/day/7

--- Day 7: Some Assembly Required ---

This year, Santa brought little Bobby Tables a set of wires and bitwise logic gates! Unfortunately, little Bobby is a little under the recommended age range, and he needs help assembling the circuit.

Each wire has an identifier (some lowercase letters) and can carry a 16-bit signal (a number from 0 to 65535). A signal is provided to each wire by a gate, another wire, or some specific value. Each wire can only get a signal from one source, but can provide its signal to multiple destinations. A gate provides no signal until all of its inputs have a signal.

The included instructions booklet describes how to connect the parts together: x AND y -> z means to connect wires x and y to an AND gate, and then connect its output to wire z.

For example:

    123 -> x means that the signal 123 is provided to wire x.
    x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
    p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then provided to wire q.
    NOT e -> f means that the bitwise complement of the value from wire e is provided to wire f.

Other possible gates include OR (bitwise OR) and RSHIFT (right-shift). If, for some reason, you'd like to emulate the circuit instead, almost all programming languages (for example, C, JavaScript, or Python) provide operators for these gates.

For example, here is a simple circuit:

123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i

After it is run, these are the signals on the wires:

d: 72
e: 507
f: 492
g: 114
h: 65412
i: 65079
x: 123
y: 456

In little Bobby's kit's instructions booklet (provided as your puzzle input), what signal is ultimately provided to wire a?

--- Part Two ---

Now, take the signal you got on wire a, override wire b to that signal, and reset the other wires (including wire a). What new signal is ultimately provided to wire a?

*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.security.MessageDigest;

// import java.text.DateFormat;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.ArrayDeque;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;

import java.util.logging.Logger;

public class aoc2015_07 implements AoC {

    final boolean __USE_LOG = false;
    final String task_description = "what shall I do?";
    final String task_input = "... with this?";
    final String[] test_input = {"test_1", "test_2"};

    public void eval (final String[] args) {
        if (args.length < 2) {help(); return;}
        String[] tis = read_input ("-");
        //String[] tis = args[1].compareTo ("-") == 0 ? task_input : args[1];
        int a = process1 (tis);
        System.out.println ("a = " + a);
        System.out.println (process2 (a, tis));
    }

    public void help() {
        print_task();
        print_input_1();
    }

    public void print_task() {
        /** print task(s) **/
        System.out.println (task_description);
    }
    
    public void print_input_test() {
        // "test" (if any) input task
        System.out.println (test_input);
    }

    public void print_output_test() {
        // "test" (if any) output task
        System.out.println ("test output");
    }

    public void print_input_1() {
        // "basic" input task
        System.out.println (task_input);
    }

    public void print_input_2() {
        // "extended" (if any) input task
        System.out.println ("task input №2");
    }

    private String[] read_input (final String src) {
        // 1x1x10
        // 2x3x4
        Scanner sc;
        // if (src == "-") ssrc = System.in; else ssrc = ...;
        sc = new Scanner (System.in);
        ArrayList <String> 号号 = new ArrayList <> ();
        for (;sc.hasNext();) {
            String 串 = sc.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 4) break;
            号号.add (串);
        }
        return 号号.toArray (new String[号号.size()]);
    }

    private int process1 (final String[] task_input) {
        SEmulator gk = new SEmulator27();
        boolean es;
        try {
            es = gk.emulate (task_input);
            gk.print();
            if (es) return gk.get_wire("a");
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    private String process2 (final int wb, final String[] task_input) {
        SEmulator gk = new SEmulator27 (wb);
        boolean es;
        try {
            es = gk.emulate (task_input);
            gk.print();
            if (es) return String.valueOf (gk.get_wire ("a"));
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
        return "done!";
    }

}


class SEmulator27 implements SEmulator {
    Map <String,SEWire> wires;
    Logger log;
    List <String> lops;

    SEmulator27() {
        wires = new HashMap <String,SEWire> ();
        lops = new LinkedList <String> ();
        log = Logger.getGlobal();
    }

    SEmulator27 (int b) {
        this();
        update_wire_map ("b", b);
    }

    public boolean emulate (final String[] ops) {
        init_wires (ops);
        for (;lops.size() > 0;) {
            log.info ("len lops " + lops.size());
            for (int 位 = lops.size() - 1; 位 >= 0; --位) {
                String 卡 = lops.get (位);
                if (apply_not(卡) ||
                    apply_mask(卡) ||
                    apply_shl(卡) ||
                    apply_shr(卡) ||
                    apply_and(卡) ||
                    apply_or(卡))
                    lops.remove (位);
            }
        }
        wires_reeval();
        return true;
    }

    public void print() {
        for (String k : wires.keySet())
            wires.get(k).print();
    }

    public int get_ip() {return -1;}

    public int get_wire (String widx) {
        if (wires.containsKey (widx))
            return wires.get(widx).value;
        else
            return -1;
    }

    private void init_wires (final String[] ops) {
        for (String op : ops)
            if (!(wire_setup(op) || wire_connect(op)))
                lops.add (op);
    }

    private void wires_reeval() {
        for (String 卡 : wires.keySet()) {
            SEWire w = wires.get (卡);
            if (w == null) continue; // oops! skip it.
            if (!w.valid) continue;
            if (w.out.isEmpty()) continue;
            for (String wo : w.out) {
                log.info ("update " + wo);
                update_wire_map (wo, w.value);
            }
        }
    }

    private boolean wire_setup (final String op) {
        Matcher m = SEmulator.pat_gate_buf.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("Buffer: " + op);
        String idx = m.group ("out");
        int val = Integer.parseInt (m.group ("value"));
        update_wire_map (idx, val);
        return true;
    }

    private boolean wire_connect (final String op) {
        Matcher m = SEmulator.pat_gate_switch.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("Switch: " + op);
        String idx = m.group ("in"),
            out = m.group ("out");
        update_wire_map (idx, out);
        return true;
    }

    private void update_wire_map (final String idx, final int val) {
        if (wires.isEmpty() || !wires.containsKey (idx))
            wires.put (idx, new SEWire (idx, val));
        else
            if (!wires.get(idx).valid)
                wires.get(idx).set(val);
    }

    private void update_wire_map (final String idx, final String out) {
        if (!wires.containsKey (out))
            wires.put (out, new SEWire (out));
        if (wires.isEmpty() || !wires.containsKey (idx))
            wires.put (idx, new SEWire (idx, out));
        else
            wires.get(idx).add(out);
    }

    private boolean apply_not (final String op) {
        Matcher m = SEmulator.pat_gate_not.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("gate: " + op);
        String in = m.group ("in"),
            out = m.group("out");
        SEWire wi = wires.get (in);
        if (wi == null || !wi.valid) return false;
        int v = wi.value;
        v = ~v;
        v &= SEmulator.MASK;
        update_wire_map (out, v);
        return true;
    }

    private boolean apply_mask (final String op) {
        Matcher m = SEmulator.pat_gate_mask.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("gate: " + op);
        String in = m.group("in"),
         out = m.group("out");
        int mask = Integer.parseInt (m.group ("mask"));
        SEWire wi = wires.get (in);
        if (wi == null || !wi.valid) return false;
        int v = wi.value;
        v &= mask;
        update_wire_map (out, v);
        return true;
    }

    private boolean apply_shl (final String op) {
        Matcher m = SEmulator.pat_gate_shl.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("gate: " + op);
        String in = m.group ("in"),
            out = m.group("out");
        int cl = Integer.parseInt (m.group ("rep"));
        SEWire wi = wires.get (in);
        if (wi == null || !wi.valid) return false;
        int v = wi.value;
        v <<= cl;
        v &= SEmulator.MASK;
        update_wire_map (out, v);
        return true;
    }

    private boolean apply_shr (final String op) {
        Matcher m = SEmulator.pat_gate_shr.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("gate: " + op);
        String in = m.group ("in"),
            out = m.group("out");
        int cl = Integer.parseInt (m.group ("rep"));
        SEWire wi = wires.get (in);
        if (wi == null || !wi.valid) return false;
        int v = wi.value;
        v >>= cl;
        update_wire_map (out, v);
        return true;
    }

    private boolean apply_and (final String op) {
        Matcher m = SEmulator.pat_gate_and.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("gate: " + op);
        String in0 = m.group ("in0"),
            in1 = m.group ("in1"),
            out = m.group("out");
        SEWire wi0 = wires.get (in0);
        if (wi0 == null || !wi0.valid) return false;
        SEWire wi1 = wires.get (in1);
        if (wi1 == null || !wi1.valid) return false;
        int v = wi0.value;
        v = v & wi1.value;
        update_wire_map (out, v);
        return true;
    }

    private boolean apply_or (final String op) {
        Matcher m = SEmulator.pat_gate_or.matcher (op);
        if (m == null || m.matches() == false) return false;
        log.info ("gate: " + op);
        String in0 = m.group ("in0"),
            in1 = m.group ("in1"),
            out = m.group("out");
        SEWire wi0 = wires.get (in0);
        if (wi0 == null || !wi0.valid) return false;
        SEWire wi1 = wires.get (in1);
        if (wi1 == null || !wi1.valid) return false;
        int v = wi0.value;
        v |= wi1.value;
        update_wire_map (out, v);
        return true;
    }
}


class SEGateBuf implements SEGate {
    /**
     * 1 -> a
     ** val=1; idx=a; out={a}
     */
    private Integer val;
    private String idx; 
    private String out;
    private SEGate gout;

    private SEGateBuf (final String in, final String out) {
        idx = out;
        this.out = out;
        val = Integer.parseInt (in);
        gout = null;
    }

    private SEGateBuf (final Integer in, final String out) {
        idx = out;
        this.out = out;
        val = in;
        gout = null;
    }

    private SEGateBuf (final int in, final String out) {
        idx = out;
        this.out = out;
        val = in;
        gout = null;
    }

    private SEGateBuf() {
        idx = null;
        this.out = null;
        val = null;
        gout = null;
    }

    public boolean hasOutput() {
        return val != null;
    }

    public static SEGate construct (final String op) {
        Matcher m = SEmulator.pat_gate_buf.matcher (op);
        if (m == null || m.matches() == false) return null;
        return new SEGateBuf (m.group ("value"), m.group ("out"));
    }

    public int join_input (SEGate in) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return (int) val;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
}


class SEGateSwitch implements SEGate {
    /**
     * a -> b
     * a -> c
     * ...
     * a -> pq
     ** val=null; in=a; out={a,b,...,pq}
     */
    private Integer val;
    //private String idx; 
    private Set <String> out_idx;
    private Set <SEGate> out;
    private SEGate in;

    private SEGateSwitch (final String in, final String out) {
        // this.idx = in;
        this.out_idx.add (out);
    }

    public static SEGate construct (final String op) {
        Matcher m = SEmulator.pat_gate_switch.matcher (op);
        if (m == null || m.matches() == false) return null;
        String wi = m.group("in"),
            wo = m.group("out");
        return new SEGateSwitch (wi, wo);
    }

    public int join_input (SEGate win) {return -1;}
    public int join_input (int val) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return -1;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
    public boolean hasOutput() {return false;} // true, if all inputs properly connected
}

class SEGateNot implements SEGate {
    /**
     * not a -> b
     */
    public static SEGate construct (String op) {return null;}
    public int join_input (SEGate in) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return -1;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
    public boolean hasOutput() {return false;} // true, if all inputs properly connected
}

class SEGateShr implements SEGate {
    /**
     * a rshift N -> b
     */
    public static SEGate construct (String op) {return null;}
    public int join_input (SEGate in) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return -1;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
    public boolean hasOutput() {return false;} // true, if all inputs properly connected
}

class SEGateShl implements SEGate {
    /**
     * a lshift N -> b
     */
    public static SEGate construct (String op) {return null;}
    public int join_input (SEGate in) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return -1;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
    public boolean hasOutput() {return false;} // true, if all inputs properly connected
}

class SEGateAnd implements SEGate {
    /**
     * a and b -> c
     * N and p -> q
     */
    public static SEGate construct (String op) {return null;}
    public int join_input (SEGate in) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return -1;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
    public boolean hasOutput() {return false;} // true, if all inputs properly connected
}

class SEGateOr implements SEGate {
    /**
     * a or b -> c
     */
    public static SEGate construct (String op) {return null;}
    public int join_input (SEGate in) {return -1;}
    public int join_output (SEGate out) {return -1;}
    public int get_output_value() {return -1;}
    public SEGate get_output() {return null;}
    public SEGate[] get_input() {return null;}
    public boolean hasOutput() {return false;} // true, if all inputs properly connected
}

