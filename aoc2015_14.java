// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-02 18:38:14 roukoru>

/*
  http://adventofcode.com/2015/day/14

--- Day 14: Reindeer Olympics ---

This year is the Reindeer Olympics! Reindeer can fly at high speeds, but must rest occasionally to recover their energy. Santa would like to know which of his reindeer is fastest, and so he has them race.

Reindeer can only either be flying (always at their top speed) or resting (not moving at all), and always spend whole seconds in either state.

For example, suppose you have the following Reindeer:

    Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
    Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.

After one second, Comet has gone 14 km, while Dancer has gone 16 km. After ten seconds, Comet has gone 140 km, while Dancer has gone 160 km. On the eleventh second, Comet begins resting (staying at 140 km), and Dancer continues on for a total distance of 176 km. On the 12th second, both reindeer are resting. They continue to rest until the 138th second, when Comet flies for another ten seconds. On the 174th second, Dancer flies for another 11 seconds.

In this example, after the 1000th second, both reindeer are resting, and Comet is in the lead at 1120 km (poor Dancer has only gotten 1056 km by that point). So, in this situation, Comet would win (if the race ended at 1000 seconds).

Given the descriptions of each reindeer (in your puzzle input), after exactly 2503 seconds, what distance has the winning reindeer traveled?

--- Part Two ---

Seeing how reindeer move in bursts, Santa decides he's not pleased with the old scoring system.

Instead, at the end of each second, he awards one point to the reindeer currently in the lead. (If there are multiple reindeer tied for the lead, they each get one point.) He keeps the traditional 2503 second time limit, of course, as doing otherwise would be entirely ridiculous.

Given the example reindeer from above, after the first second, Dancer is in the lead and gets one point. He stays in the lead until several seconds into Comet's second burst: after the 140th second, Comet pulls into the lead and gets his first point. Of course, since Dancer had been in the lead for the 139 seconds before that, he has accumulated 139 points by the 140th second.

After the 1000th second, Dancer has accumulated 689 points, while poor Comet, our old champion, only has 312. So, with the new scoring system, Dancer would win (if the race ended at 1000 seconds).

Again given the descriptions of each reindeer (in your puzzle input), after exactly 2503 seconds, how many points does the winning reindeer have?

*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_14 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;

    final static int TTR = 2503;

    final String[] test_input = {
        "runtime 1000",
        "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.",
        "Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds."
    };

    final String[] data_input = {
        "Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.",
        "Rudolph can fly 3 km/s for 15 seconds, but then must rest for 28 seconds.",
        "Donner can fly 19 km/s for 9 seconds, but then must rest for 164 seconds.",
        "Blitzen can fly 19 km/s for 9 seconds, but then must rest for 158 seconds.",
        "Comet can fly 13 km/s for 7 seconds, but then must rest for 82 seconds.",
        "Cupid can fly 25 km/s for 6 seconds, but then must rest for 145 seconds.",
        "Dasher can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.",
        "Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.",
        "Prancer can fly 25 km/s for 6 seconds, but then must rest for 143 seconds."
    };

    public void help() {
        System.out.println
            ("java aoc2015 14 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args);
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
        return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private String[] read_lines (final String src) throws IOException {
        /**
         * read lines from stdin (src="-") or other file
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 7)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[][] read_input (final String src) throws IOException {
        /**
         * read lines from src and converts them to int triplets
         * 1x1x10
         * 2x3x4
         */
        Scanner 输入流 = get_input_scanner (src);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 号.length; ++甲) // && 甲 < 字.length
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin"))
                    串川 = data_input;
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    final Pattern dpat = Pattern.compile ("\\w+ can fly (?<speed>\\d+) km/s for (?<fly>\\d+) seconds, but then must rest for (?<rest>\\d+) seconds.");
    final Pattern rtpat = Pattern.compile ("runtime (\\d+)");

    private InputData parse_input (final String[] indata) {
        /**
         * Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
         * Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
         */
        InputData 桂马 = new InputData (); // gui ma
        ArrayList <Deer> 鹿 = new ArrayList <Deer> ();
        int 速, 飞, 休;
        Matcher 配; // pèi
        for (String 串 : indata) {
            配 = rtpat.matcher (串);
            if (配 != null && 配.matches()) {
                桂马.runtime = Integer.parseInt (配.group (1));
                continue;
            }
            配 = dpat.matcher (串);
            if (配 == null || !配.matches()) continue;
            速 = Integer.parseInt (配.group ("speed"));
            飞 = Integer.parseInt (配.group ("fly"));
            休 = Integer.parseInt (配.group ("rest"));
            鹿.add (new Deer (速, 飞, 休));
        }
        桂马.鹿 = 鹿.toArray(new Deer[0]);
        return 桂马;
    }

    private int process1 (final InputData 任务) {
        int 长 = 0;
        for (Deer 鹿 : 任务.鹿) {
            // System.out.println (鹿);
            int 甲 = 鹿.run (任务.runtime);
            if (甲 > 长)
                长 = 甲;
        }
        return 长;
    }

    private int process2 (final InputData 任务) {
        Race 比赛 = new Race (任务); // bǐsài
        比赛.rewind();
        return 比赛.run();
    }
}


class InputData {
    Deer[] 鹿;
    int runtime;
    InputData() {
        鹿 = null;
        runtime = aoc2015_14.TTR;
    }
}


class Deer {
    /**
     * Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
     */

    //final String 名字;
    final int 速度, 飞分, 休分;

    Deer (final int speed, final int fly, final int rest) {
        //名字 = null;
        速度 = speed; // su du
        飞分 = fly;
        休分 = rest; // xi
    }

    public int run (final int 米分) {
        /**
         * returns distance covered by this deer by 米分 seconds
         */
        int 道, 时;
        for (道 = 0, 时 = 米分; 时 > 0; 时 -= 休分) {
            if (时 >= 飞分) {
                道 += 速度 * 飞分;
                时 -= 飞分;
            }
            else {
                道 += 速度 * 时 / 飞分;
                时 = 0;
            }
        }
        return 道;
    }

    public String toString() {
        return String.format ("%d %d %d", 速度, 飞分, 休分);
    }
}


class Race {
    final boolean STATE_REST = true, STATE_RUN = false;
    int runtime;
    final Deer[] 鹿; // lu
    int[] 长, 比分, 度;
    boolean[] 休;

    Race (final InputData 任务){
        鹿 = 任务.鹿;
        长 = new int [任务.鹿.length];
        休 = new boolean [任务.鹿.length];
        比分 = new int [任务.鹿.length];
        度 = new int [任务.鹿.length];
        runtime = 任务.runtime;
        rewind();
    }

    public void rewind() {
        Arrays.fill (长, 0);
        Arrays.fill (休, STATE_REST);
        Arrays.fill (比分, 0);
        Arrays.fill (度, 0);
    }

    public int run() {
        for (int 分 = 0; 分 < runtime; ++分) {
            by1_step();
            scoring();
            //System.out.println ("step " + 分); print();
        }
        System.out.println (Arrays.toString(比分));
        return get_max (比分);
    }

    private int get_max (int[] 表) {
        int 甲 = 0;
        for (int 号 : 表)
            if (号 > 甲)
                甲 = 号;
        return 甲;
    }

    private void by1_step() {
        for (int 只 = 0; 只 < 鹿.length; ++只) {
            if (--度[只] <= 0) {
                休[只] = !休[只];
                度[只] = 休[只] ? 鹿[只].休分 : 鹿[只].飞分;
                // System.out.printf ("a %d is now %s for %d s\n", 只, 休[只] ? "resting" : "running", 度[只]);
                // System.out.printf ("lead %d\n", leader);
            }
            if (!休[只]) { // running
                长[只] += 鹿[只].速度;
            }
        }
    }

    //private int leader = -1;
    private void scoring() {
        int 甲 = get_max (长);
        for (int 只 = 0; 只 < 鹿.length; ++只)
            if (长[只] == 甲) {
                ++比分[只];
                //leader = 只;
            }
    }

    public void print() {
        System.out.printf ("distance %s\n", Arrays.toString (长));
        System.out.printf ("time cnt %s\n", Arrays.toString (度));
        System.out.printf ("resting  %s\n", Arrays.toString (休));
        System.out.printf ("score    %s\n", Arrays.toString (比分));
    }
}
