// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// --- Time-stamp: <2018-11-24 16:26:40 roukoru>

// package aoc2015;

interface AoC {
    public void eval (final String[] args) throws Exception;
    public void help();
    /*
    public void print_task(); // print task(s)
    public void print_input_test(); // "test" (if any) input task
    public void print_output_test(); // "test" (if any) output task
    public void print_input_1(); // "basic" input task
    public void print_input_2(); // "extended" (if any) input task
    */
}
