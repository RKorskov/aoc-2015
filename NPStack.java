// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-02 23:23:54 roukoru>

// package aoc2015;

import java.util.NoSuchElementException;
import java.util.Arrays;

class NPStack {
    /**
     * minimal fixed-size stack for P/NP tasks in AoC
     */

    int[] 目录; // mù lù
    int 量; // liàng

    NPStack() {目录 = null;}

    NPStack (final int guests) {
        this.目录 = new int [guests];
        量 = 0;
        Arrays.fill (目录, -1);
    }

    public void clear() {量 = 0;Arrays.fill (目录, -1);}
    public int size() {return 量;}
    public String toString() {return Arrays.toString (目录);}

    public void add (final int 客) throws NoSuchElementException {push (客);}

    public void push (final int 客) throws NoSuchElementException {
        if (量 < 0) 量 = 0;
        if (量 < 目录.length)
            目录[量++] = 客;
        else
            throw new NoSuchElementException ("stack is full");
    }

    public int pop() throws NoSuchElementException {
        if (量 < 1 || 量 > 目录.length)
            throw new NoSuchElementException ("stack is empty");
        return 目录[--量];
    }

    public int peek() throws NoSuchElementException {
        if (量 < 1 || 量 > 目录.length)
            throw new NoSuchElementException ("stack is empty");
        return 目录[量-1];
    }

    public int peek (final int 位) throws NoSuchElementException {
        if (位 < 0 || 位 >= 量)
            throw new NoSuchElementException ("no such index in the stack");
        return 目录[位];
    }

    public int prev (final int 位) throws NoSuchElementException {
        if (位 < 0 || 位 >= 量)
            throw new NoSuchElementException ("no such index in the stack");
        int 甲 = 位 - 1;
        if (甲 < 0) 甲 = 量 - 1;
        return 目录[甲];
    }

    public int next (final int 位) throws NoSuchElementException {
        if (位 < 0 || 位 >= 量)
            throw new NoSuchElementException ("no such index in the stack");
        int 甲 = 位 + 1;
        if (甲 >= 量) 甲 = 0;
        return 目录[甲];
    }

    public boolean contains (final int 客) {
        for (int 位 = 0; 位 < 量; ++位)
            if (客 == 目录[位])
                return true;
        return false;
    }
}
