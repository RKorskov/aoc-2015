// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-18 09:42:08 roukoru>

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

interface SEmulator {
    public boolean emulate (String[] ops);
    public void print();
    public int get_ip();
    public int get_wire (String widx);

    final int MASK = 0xFFFF;
    public final Pattern pat_gate_buf = Pattern.compile ("^(?<value>\\d+)\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_switch = Pattern.compile ("^(?<in>[a-z]{1,2}+)\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_and = Pattern.compile ("^(?<in0>[a-z]{1,2})\\s+AND\\s+(?<in1>[a-z]{1,2})\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_mask = Pattern.compile ("^(?<mask>\\d{1,5})\\s+AND\\s+(?<in>[a-z]{1,2})\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_or = Pattern.compile ("^(?<in0>[a-z]{1,2})\\s+OR\\s+(?<in1>[a-z]{1,2})\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_shl = Pattern.compile ("^(?<in>[a-z]{1,2})\\s+LSHIFT\\s+(?<rep>\\d+)\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_shr = Pattern.compile ("^(?<in>[a-z]{1,2})\\s+RSHIFT\\s+(?<rep>\\d+)\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE),
        pat_gate_not = Pattern.compile ("^NOT\\s+(?<in>[a-z]{1,2})\\s*->\\s*(?<out>[a-z]{1,2})$", Pattern.CASE_INSENSITIVE);
}
