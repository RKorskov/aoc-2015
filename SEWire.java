// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-18 09:47:30 roukoru>

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

class SEWire {
    int value;
    boolean valid;
    String index;
    Set <String> out;

    SEWire() {
        value = -1;
        valid = false;
        index = null;
        out = new HashSet <String> ();
    }

    SEWire (final String idx) {
        this();
        index = idx;
    }

    SEWire (final String idx, final String out) {
        /**
         * a -> b
         */
        this();
        index = idx;
        this.out.add (out);
    }

    SEWire (final String idx, final int val) {
        /**
         * 2 -> b
         */
        this();
        value = val;
        valid = true;
        index = idx;
    }

    public void add (final String out) {
        if (this.out == null) this.out = new HashSet <String> ();
        this.out.add (out);
    }

    public void set (final int val) {
        value = val;
        valid = true;
    }

    public void print () {
        System.out.printf ("%s = %d : %s\n", index, value,
                           Arrays.toString (out.toArray (new String [out.size()])));
    }
}
