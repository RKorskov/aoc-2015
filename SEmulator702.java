// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-17 23:03:53 roukoru>

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

class SEmulator702 implements SEmulator {
     // A gate provides no signal until all of its inputs have a signal.
    Map <String,Integer> wires; // a..z, aa..zz
    int ip;

    SEmulator702() {
        wires = new TreeMap <String,Integer> ();
        ip = 0;
    }

    public boolean emulate (final String[] ops) {
        for (String op : ops)
            if (gate_and (op) ||
                gate_or (op)  ||
                gate_shl (op) ||
                gate_shr (op) ||
                gate_not (op) ||
                gate_set (op)) {++ip; continue;}
            else return false;
        return true;
    }

    public void print() {
        for (String k : wires.keySet())
            System.out.printf ("%s : %d\n", k, (int)wires.get(k));
    }

    public int get_ip() {return ip;}

    public int get_wire (String widx) {
        if (wires.isEmpty() || !wires.containsKey (widx)) return -1;
        return wires.get (widx);
    }

    private boolean gate_set (final String op) {
        // Number -> wire_out
        // wire_in -> wire_out
        Matcher m = pat_gate_buf.matcher (op);
        if (m != null && m.matches()) {
            Integer n = Integer.parseInt (m.group ("value"));
            String wo = m.group("out");
            wires.put (wo, n);
            return true;
        }

        m = pat_gate_switch.matcher (op);
        if (m == null || m.matches() == false) return false;
        String wi = m.group("in"),
            wo = m.group("out");
        Integer n = wires.get (wi);
        wires.put (wo, n);
        return true;
    }

    private boolean gate_not (final String op) {
        Matcher m = pat_gate_not.matcher (op);
        if (m == null || m.matches() == false) return false;
        String wi = m.group("in"),
            wo = m.group("out");
        int n = wires.get (wi);
        n = ~n;
        n &= MASK;
        wires.put (wo, n);
        return true;
    }

    private boolean gate_shl (final String op) {
        Matcher m = pat_gate_shl.matcher (op);
        if (m == null || m.matches() == false) return false;
        int cl = Integer.parseInt (m.group ("rep"));
        String wi = m.group("in"),
            wo = m.group("out");
        int n = wires.get (wi);
        n <<= cl;
        n &= MASK;
        wires.put (wo, n);
        return true;
    }

    private boolean gate_shr (final String op) {
        Matcher m = pat_gate_shr.matcher (op);
        if (m == null || m.matches() == false) return false;
        int cl = Integer.parseInt (m.group ("rep"));
        String wi = m.group("in"),
            wo = m.group("out");
        int n = wires.get (wi);
        n &= MASK;
        n >>= cl;
        wires.put (wo, n);
        return true;
    }

    private boolean gate_and (final String op) {
        Matcher m = pat_gate_and.matcher (op);
        if (m == null || m.matches() == false) return false;
        String wi0 = m.group("in0"),
            wi1 = m.group("in1"),
            wo = m.group("out");
        int p = wires.get (wi0),
            q = wires.get (wi1);
        p &= q;
        p &= MASK;
        wires.put (wo, p);
        return true;
    }

    private boolean gate_or (final String op) {
        Matcher m = pat_gate_or.matcher (op);
        if (m == null || m.matches() == false) return false;
        String wi0 = m.group("in0"),
            wi1 = m.group("in1"),
            wo = m.group("out");
        int p = wires.get (wi0),
            q = wires.get (wi1);
        p |= q;
        p &= MASK;
        wires.put (wo, p);
        return true;
    }

}
