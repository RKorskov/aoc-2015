// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-24 01:30:33 roukoru>

/*
  http://adventofcode.com/2015/day/11

--- Day 11: Corporate Policy ---

Santa's previous password expired, and he needs help choosing a new one.

To help him remember his new password after the old one expires, Santa has devised a method of coming up with a password based on the previous one. Corporate policy dictates that passwords must be exactly eight lowercase letters (for security reasons), so he finds his new password by incrementing his old password string repeatedly until it is valid.

Incrementing is just like counting with numbers: xx, xy, xz, ya, yb, and so on. Increase the rightmost letter one step; if it was z, it wraps around to a, and repeat with the next letter to the left until one doesn't wrap around.

Unfortunately for Santa, a new Security-Elf recently started, and he has imposed some additional password requirements:

    Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
    Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
    Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.

For example:

    hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
    abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
    abbcegjk fails the third requirement, because it only has one double letter (bb).
    The next password after abcdefgh is abcdffaa.
    The next password after ghijklmn is ghjaabcc, because you eventually skip all the passwords that start with ghi..., since i is not allowed.

Given Santa's current password (your puzzle input), what should his next password be?

Your puzzle input is vzbxkghb
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

// import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.LinkedList;
// import java.util.List;
// import java.util.Map;
// import java.util.Scanner;
// import java.util.Set;
// import java.util.TreeMap;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2015_11 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String task_input = "vzbxkghb";

    public void help() {;}

    public void eval (final String[] args) { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        if (args.length < 1) return;
        String tis = args[1].equals ("-") ? task_input : args[1];
        String 串 = process1 (tis);
        System.out.println (串);
        System.out.println (process2 (串));
    }

    private String process1 (final String task_input) {
        final long 全字 = 208_827_064_576L; // 26^8 == 208_827_064_576
        //log.info (task_input);
        byte[] 密码 = task_input.getBytes(); // mìmǎ
        for (long 甲 = 0; 甲 < 全字; ++甲) {
            inc (密码);
            //log.info (new String (密码));
            if (rule_1(密码) && rule_2(密码) && rule_3(密码))
                return new String (密码);
        }
        return null;
    }

    private String process2 (final String task_input) {
        return process1 (task_input);
    }

    private void inc (final byte[] pwd) {
        boolean 借 = true;
        for (int 位 = pwd.length - 1; 位 >= 0; --位) {
            byte 字 = pwd[位];
            if (借) ++字;
            if (字 <= 'z') {
                pwd[位] = 字;
                break;
            }
            pwd[位] = 'a';
            借 = true;
            if (位 == 0)
                位 = pwd.length;
        }
    }

    private boolean rule_1 (final byte[] pwd) {
        /**
         * Passwords must include one increasing straight of at least
         * three letters, like abc, bcd, cde, and so on, up to
         * xyz. They cannot skip letters; abd doesn't count.
         */
        byte 前 = pwd[0], 今 = pwd[1];
        for (int 位 = 2; 位 < pwd.length; ++位) {
            if ((前+2) == (今+1) && (今+1) == pwd[位])
                return true;
            else {
                前 = 今;
                今 = pwd[位];
            }
        }
        return false;
    }

    private boolean rule_2 (final byte[] pwd) {
        /**
         * Passwords may not contain the letters i, o, or l, as these
         * letters can be mistaken for other characters and are
         * therefore confusing.
         */
        for (byte 字 : pwd)
            if (字 == 'i' || 字 == 'o' || 字 == 'l')
                return false;
        return true;
    }

    private boolean rule_3 (final byte[] pwd) {
        /**
         * Passwords must contain at least two different,
         * non-overlapping pairs of letters, like aa, bb, or zz.
         * e.g. bbcc or bbbb
         */
        int 位 = rule_3 (pwd, 0);
        if (位 >= 0 && rule_3 (pwd, 位 + 2) > 位)
            return true;
        return false;
    }

    private int rule_3 (final byte[] pwd, final int from) {
        /**
         * Passwords must contain at least two different,
         * non-overlapping pairs of letters, like aa, bb, or zz.
         * e.g. bbcc or bbbb
         */
        if (from > (pwd.length-2)) return -1;
        byte 前 = pwd[from];
        for (int 位 = from + 1; 位 < pwd.length; ++位)
            if (前 == pwd[位])
                return 位 - 1;
            else
                前 = pwd[位];
        return -1;
    }
}
