// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-14 15:26:48 roukoru>

/*
  http://adventofcode.com/2015/day/4

--- Day 4: The Ideal Stocking Stuffer ---

Santa needs help mining some AdventCoins (very similar to bitcoins) to use as gifts for all the economically forward-thinking little girls and boys.

To do this, he needs to find MD5 hashes which, in hexadecimal, start with at least five zeroes. The input to the MD5 hash is some secret key (your puzzle input, given below) followed by a number in decimal. To mine AdventCoins, you must find Santa the lowest positive number (no leading zeroes: 1, 2, 3, ...) that produces such a hash.

For example:

    If your secret key is abcdef, the answer is 609043, because the MD5 hash of abcdef609043 starts with five zeroes (000001dbbfa...), and it is the lowest such number to do so.
    If your secret key is pqrstuv, the lowest number it combines with to make an MD5 hash starting with five zeroes is 1048970; that is, the MD5 hash of pqrstuv1048970 looks like 000006136ef....

Your puzzle input is iwrupvqb.

--- Part Two ---

Now find one that starts with six zeroes.
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
import java.util.Scanner;
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;

import java.util.logging.Logger;

public class aoc2015_04 implements AoC {

    final boolean __USE_LOG = false;
    final String task_description = "what shall I do?";
    final String task_input = "iwrupvqb";
    final String[] test_input = {"abcdef", "pqrstuv"};
    final String[] test_output = {"609043", "1048970"};

    public void eval (final String[] args) {
        if (args.length < 2) {help(); return;}
        // int[][] tis = read_input ("-");
        if (args.length > 2) {
            byte[] b = md5sum (args[1], Integer.parseInt (args[2]));
            for (byte a : b)
                System.out.printf ("%02x", a);
            System.out.println (check5Z (b));
        }
        else {
            // todo: have to be rewriten in more clean way...
            System.out.println (process1 (args[1])); // task_input
            System.out.println (process2 (args[1])); // task_input
        }
    }

    public void help() {
        print_task();
    }

    public void print_task() {
        /** print task(s) **/
        System.out.println (task_description);
    }
    
    public void print_input_test() {
        // "test" (if any) input task
        System.out.println ("test input");
    }

    public void print_output_test() {
        // "test" (if any) output task
        System.out.println ("test output");
    }

    public void print_input_1() {
        // "basic" input task
        System.out.println (task_input);
    }

    public void print_input_2() {
        // "extended" (if any) input task
        System.out.println ("task input №2");
    }

    private byte[] md5sum (final String salt, final int value) {
        MessageDigest md5;
        try {md5 = MessageDigest.getInstance ("MD5");}
        catch (Exception ex) {return null;} // NoSuchAlgorithmException
        md5.reset();
        String 串 = salt + value;
        return md5.digest (串.getBytes());
    }

    private int check5Z (final byte[] hash) {
        return hash[0] | hash[1] | hash[2] & 0xF0;
    }

    private int check6Z (final byte[] hash) {
        return hash[0] | hash[1] | hash[2];
    }

    private int process1 (final String task_input) {
        Logger log;
        log = Logger.getGlobal();
        int 号, 丁;
        for (丁 = 1, 号 = 1; 号 < 0x7FFF_FFFF; ++号, ++丁) {
            byte[] hash = md5sum (task_input, 号);
            if (check5Z (hash) == 0) break;
            if (丁 >= 10000) {
                丁 = 0;
                log.info (task_input + 号);
            }
        }
        return 号;
    }

    private int process2 (final String task_input) {
        Logger log;
        log = Logger.getGlobal();
        int 号, 丁;
        for (丁 = 1, 号 = 1; 号 < 0x7FFF_FFFF; ++号, ++丁) {
            byte[] hash = md5sum (task_input, 号);
            if (check6Z (hash) == 0) break;
            if (丁 >= 10000) {
                丁 = 0;
                log.info (task_input + 号);
            }
        }
        return 号;
    }
}
