// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-11-14 17:27:55 roukoru>

/*
  http://adventofcode.com/2015/day/5

--- Day 5: Doesn't He Have Intern-Elves For This? ---

Santa needs help figuring out which strings in his text file are naughty or nice.

A nice string is one with all of the following properties:

    It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
    It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
    It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.

For example:

    ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
    aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
    jchzalrnumimnmhp is naughty because it has no double letter.
    haegwjzuvuyypxyu is naughty because it contains the string xy.
    dvszwmarrgswjxmb is naughty because it contains only one vowel.

How many strings are nice?

--- Part Two ---

Realizing the error of his ways, Santa has switched to a better model of determining whether a string is naughty or nice. None of the old rules apply, as they are all clearly ridiculous.

Now, a nice string is one with all of the following properties:

    It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
    It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.

For example:

    qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
    xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each rule overlap.
    uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
    ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo), but no pair that appears twice.

How many strings are nice under these new rules?
*/

// package aoc2015;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.security.MessageDigest;

// import java.text.DateFormat;

import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
import java.util.Scanner;
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;

import java.util.logging.Logger;

public class aoc2015_05 implements AoC {

    final boolean __USE_LOG = false;
    final String task_description = "what shall I do?";
    final String task_input = "... with this?";
    final String[] test_input = {"ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb"};
    final boolean[] test_output = {true, true, false, false, false};

    public void eval (final String[] args) {
        if (args.length < 2) {help(); return;}
        String[] tis = read_input ("-");
        //String tis = args[1].compareTo ("-") == 0 ? task_input : args[1];
        System.out.println (process1 (tis));
        System.out.println (process2 (tis));
    }

    public void help() {
        print_task();
        print_input_1();
    }

    public void print_task() {
        /** print task(s) **/
        System.out.println (task_description);
    }
    
    public void print_input_test() {
        // "test" (if any) input task
        System.out.println ("test input");
    }

    public void print_output_test() {
        // "test" (if any) output task
        System.out.println ("test output");
    }

    public void print_input_1() {
        // "basic" input task
        System.out.println (task_input);
    }

    public void print_input_2() {
        // "extended" (if any) input task
        System.out.println ("task input №2");
    }

    private String[] read_input (final String src) {
        Scanner sc;
        // if (src == "-") ssrc = System.in; else ssrc = ...;
        sc = new Scanner (System.in);
        ArrayList <String> 表 = new ArrayList <> (); // biao
        for (;sc.hasNext();) {
            String 串 = sc.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null) break;
            表.add (串);
        }
        return 表.toArray (new String[表.size()]);
    }

    private boolean is_vowel (final char c) {
        switch (c) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return true;
        }
        return false;
    }

    private boolean check_vowels (final String str) {
        // It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
        int 号 = 0;
        for (int 位 = 0; 位 < str.length(); ++位)
            if (is_vowel (str.charAt (位))) ++号;
        return 号 >= 3;
    }

    private boolean check_dups (final String str) {
        // It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd)
        for (int 位 = 1; 位 < str.length(); ++位)
            if (str.charAt(位) == str.charAt(位-1))
                return true;
        return false;
    }

    private boolean check_subs (final String str) {
        // It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements
        for (int 位 = 1; 位 < str.length(); ++位) {
            String 串 = str.substring (位-1, 位+1);
            switch (串) {
            case "ab":
            case "cd":
            case "pq":
            case "xy":
                return false;
            }
        }
        return true;
    }

    private int process1 (final String[] src_list) {
        int 号 = 0;
        for (String 串 : src_list) {
            if (check_vowels (串) && check_dups (串) && check_subs (串))
                ++号;
        }
        return 号;
    }

    private boolean check_pairs (final String str) {
        // It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps)
        String 节;
        int 号;
        for (int 位 = 1; 位 < str.length() - 2; ++位) {
            节 = str.substring (位 - 1, 位 + 1); // jie
            号 = str.indexOf (节, 位 + 1);
            if (号 > 位) return true;
        }
        return false;
    }

    private boolean check_cxc (final String str) {
        // It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa
        for (int 位 = 2; 位 < str.length(); ++位)
            if (str.charAt (位) == str.charAt (位 - 2))
                return true;
        return false;
    }

    private int process2 (final String[] src_list) {
        int 号 = 0;
        for (String 串 : src_list) {
            if (check_pairs (串) && check_cxc (串))
                ++号;
        }
        return 号;
    }

}
